﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfEnrolServiceLibrary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    [ServiceBehavior(InstanceContextMode=InstanceContextMode.Single)]
    public class EnrolService : IEnrolService
    {
        List<Person> people;

        public EnrolService()
        {
            people = new List<Person>();
        }
        public void Enrol(Person value)
        {
            people.Add(value);
        }

        public Person[] GetEnrolled()
        {
            return people.ToArray();
        }
    }
}
