﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcTester
{
    class Program
    {
        static void Main(string[] args)
        {
            CalcServiceReference.CalcServiceClient client = new CalcServiceReference.CalcServiceClient();

            double suma = client.Add(1, 4);
            double roznica = client.Sub(8, 4);
            double iloczyn = client.Mul(9, 3);
            double iloraz = client.Dev(6, 4);
            
            client.Close();
            Console.ReadKey();
        }
    }
}
