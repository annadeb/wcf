﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CalcTester.CalcServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="CalcServiceReference.ICalcService")]
    public interface ICalcService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalcService/Add", ReplyAction="http://tempuri.org/ICalcService/AddResponse")]
        double Add(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalcService/Add", ReplyAction="http://tempuri.org/ICalcService/AddResponse")]
        System.Threading.Tasks.Task<double> AddAsync(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalcService/Sub", ReplyAction="http://tempuri.org/ICalcService/SubResponse")]
        double Sub(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalcService/Sub", ReplyAction="http://tempuri.org/ICalcService/SubResponse")]
        System.Threading.Tasks.Task<double> SubAsync(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalcService/Mul", ReplyAction="http://tempuri.org/ICalcService/MulResponse")]
        double Mul(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalcService/Mul", ReplyAction="http://tempuri.org/ICalcService/MulResponse")]
        System.Threading.Tasks.Task<double> MulAsync(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalcService/Dev", ReplyAction="http://tempuri.org/ICalcService/DevResponse")]
        double Dev(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalcService/Dev", ReplyAction="http://tempuri.org/ICalcService/DevResponse")]
        System.Threading.Tasks.Task<double> DevAsync(double a, double b);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ICalcServiceChannel : CalcTester.CalcServiceReference.ICalcService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CalcServiceClient : System.ServiceModel.ClientBase<CalcTester.CalcServiceReference.ICalcService>, CalcTester.CalcServiceReference.ICalcService {
        
        public CalcServiceClient() {
        }
        
        public CalcServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CalcServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CalcServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CalcServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public double Add(double a, double b) {
            return base.Channel.Add(a, b);
        }
        
        public System.Threading.Tasks.Task<double> AddAsync(double a, double b) {
            return base.Channel.AddAsync(a, b);
        }
        
        public double Sub(double a, double b) {
            return base.Channel.Sub(a, b);
        }
        
        public System.Threading.Tasks.Task<double> SubAsync(double a, double b) {
            return base.Channel.SubAsync(a, b);
        }
        
        public double Mul(double a, double b) {
            return base.Channel.Mul(a, b);
        }
        
        public System.Threading.Tasks.Task<double> MulAsync(double a, double b) {
            return base.Channel.MulAsync(a, b);
        }
        
        public double Dev(double a, double b) {
            return base.Channel.Dev(a, b);
        }
        
        public System.Threading.Tasks.Task<double> DevAsync(double a, double b) {
            return base.Channel.DevAsync(a, b);
        }
    }
}
