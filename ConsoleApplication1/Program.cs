﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceReference1.ServiceLabClient client = new ServiceReference1.ServiceLabClient();
            Console.WriteLine(client.Hello());
            byte[] tab = client.Image();
            SaveImage(tab, "obrazek.png");

            Console.WriteLine(client.Image());
            client.Close();
            Console.ReadKey();
        }
        static void SaveImage(byte[] image, string path)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            ms.Write(image, 0, image.Length);
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);
            bmp.Save(path);
        }
          
    }
}
